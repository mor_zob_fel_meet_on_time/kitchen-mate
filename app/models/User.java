package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.*;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;

@Entity
@Table(name = "account")
public class User extends Model {

    private static final long serialVersionUID = -8022939500507972550L;

    @Required
    public String name;

    @Required
    public String vorname;

    @Id
    @Required
    @Email
    public String email;

    @Required
    public String password;

    public static Model.Finder<String, User> find = new Model.Finder<String, User>(
	    String.class, User.class);

    /**
     * Authenticate a User.
     */
    public static User authenticate(String email, String password) {
	return find.where().eq("email", email).eq("password", password)
		.findUnique();
    }

}
