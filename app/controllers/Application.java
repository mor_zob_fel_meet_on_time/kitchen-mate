package controllers;

import play.mvc.Controller;
import play.mvc.Result;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class Application extends Controller {

    @Inject
    public Application() {
    }

    public static Result index() {
	return redirect(routes.Login.login());
    }

}
