package controllers;

import static play.data.Form.form;
import models.User;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.login;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class Login extends Controller {

    @Inject
    public Login() {
    }

    public Result showRegistration() {
	return ok(views.html.registration.render(createUserForm()));
    }

    public Result login() {
	return ok(views.html.login.render(createAuthForm()));
    }

    public Result logout() {
	session().clear();
	flash("success", "You've been logged out");
	return redirect(routes.Login.login());
    }

    public Result executeRegistration() {
	Form<User> userForm = createUserForm().bindFromRequest();
	if (userForm.hasErrors()) {
	    return handleBadRequest();
	}
	User user = userForm.get();
	user.save();
	return redirect(routes.Login.login());
    }

    /**
     * Handle login form submission.
     */
    public Result authenticate() {
	Form<Auth> authForm = createAuthForm().bindFromRequest();
	if (authForm.hasErrors()) {
	    return badRequest(login.render(authForm));
	} else {
	    session("email", authForm.get().email);
	    return redirect(routes.Menu.proposeMenu());
	}
    }


    Status handleBadRequest() {
	return badRequest(views.html.registration.render(createUserForm()));
    }

    Form<User> createUserForm() {
	return form(User.class);
    }

    Form<Auth> createAuthForm() {
	return form(Auth.class);
    }

}
