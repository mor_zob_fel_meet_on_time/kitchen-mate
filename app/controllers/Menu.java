package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security.Authenticated;
import utils.Helper;
import auth.Secured;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Authenticated(Secured.class)
public class Menu extends Controller {

    @Inject
    public Menu() {

    }

    public Result proposeMenu() {
	return ok(views.html.proposeMenu.render(Helper.showLogText(session())));
    }

}
