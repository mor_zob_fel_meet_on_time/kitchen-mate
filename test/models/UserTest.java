package models;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.*;
import static play.test.Helpers.*;
import helper.BaseModelTest;
import models.User;

import org.junit.Test;

public class UserTest extends BaseModelTest {

    @Test
    public void testSaveUserAndAuthenticateIt() throws Exception {
	User user = createJohnDoe();
	user.save();
	User savedUser = User.authenticate(user.email, user.password);
	assertThat(savedUser).isNotNull();
	assertThat(savedUser.vorname).isEqualTo(user.vorname);
    }

}
