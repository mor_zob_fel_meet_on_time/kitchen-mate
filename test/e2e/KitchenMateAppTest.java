package e2e;

import static org.fest.assertions.Assertions.assertThat;
import static org.fluentlenium.core.filter.FilterConstructor.*;
import static play.test.Helpers.HTMLUNIT;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;

import java.util.HashMap;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.internal.seleniumemulation.Click;

import com.gargoylesoftware.htmlunit.javascript.host.Element;

import play.api.mvc.Filter;
import play.libs.F.Callback;
import play.test.TestBrowser;
import controllers.routes;
import static org.fluentlenium.core.filter.FilterConstructor.*;
import static org.junit.Assert.*;

public class KitchenMateAppTest {

    @Test
    @Ignore
    public void testRegistration() throws Exception {
	HashMap<String, Object> dbSettings = new HashMap<String, Object>();
	dbSettings.put("db.default.url",
		"jdbc:mysql//localhost:3306/kitchenmatetest");

	running(testServer(3333, fakeApplication()), HTMLUNIT,
		new Callback<TestBrowser>() {

		    @Override
		    public void invoke(TestBrowser browser) throws Throwable {
			browser.goTo("http://localhost:3333"
				+ routes.Application.index().url());
			assertThat(browser.title()).isEqualTo("Kitchen Mate");
			assertThat(browser.pageSource()).contains(
				"Registrieren");
			assertThat(browser.url()).isEqualTo(
				"http://localhost:3333/login");
			browser.$("a").click();
			assertThat(browser.url()).isEqualTo(
				"http://localhost:3333/showRegisration");
			assertThat(browser.pageSource()).contains("Vorname");
			assertThat(browser.pageSource()).contains("Name");
			assertThat(browser.pageSource()).contains("Email");
			assertThat(browser.pageSource()).contains("password");

			browser.fill("#val_vorname").with("John");
			browser.fill("#val_nachname").with("Doe");
			browser.fill("#val_email").with("john.doe@doe.com");
			browser.fill("#val_password").with("secret");

			browser.$("#registrieren").click();

			assertThat(browser.url()).isEqualTo(
				"http://localhost:3333/login");

			browser.fill("#val_email").with("john.doe@doe.com");
			browser.fill("#val_password").with("secret");
			browser.$("#login").click();

			assertThat(browser.url()).isEqualTo(
				"http://localhost:3333/vorschlag");

			assertThat(browser.pageSource()).contains("John");
			assertThat(browser.pageSource()).contains("Doe");
			assertThat(browser.pageSource()).contains(
				"john.doe@doe.com");

		    }
		});
    }

    @Test
    public void test() throws Exception {

	running(testServer(3333, fakeApplication()), HTMLUNIT,
		new Callback<TestBrowser>() {

		    @Override
		    public void invoke(TestBrowser browser) throws Throwable {
			browser.goTo("http://localhost:3333"
				+ routes.Application.index().url());
			// browser.click("a", By.id(""));
			assertThat(browser.url()).isEqualTo(
				"http://localhost:3333/showRegistration");

		    }
		});
    }

}
