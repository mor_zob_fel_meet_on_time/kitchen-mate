package controllers;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.GET;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.routeAndCall;
import static play.test.Helpers.status;

import org.junit.Test;

import play.mvc.Http;
import play.mvc.Result;


public class ApplicationTest {

    @Test
    public void testRouteToLogin() throws Exception {
	Result result = routeAndCall(fakeRequest(GET, "/"));
	assertThat(status(result)).isEqualTo(Http.Status.SEE_OTHER);
    }

}
