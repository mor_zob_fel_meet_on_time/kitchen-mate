package controllers;

import static org.fest.assertions.Assertions.assertThat;
import helper.BaseModelTest;
import models.User;

import org.junit.Test;

public class AuthTest extends BaseModelTest {

    @Test
    public void testAuthenticateValidUser() throws Exception {
	User user = createJohnDoe();
	user.save();

	Auth auth = new Auth();
	auth.email = user.email;
	auth.password = user.password;

	assertThat(auth.validate()).isNull();
    }

    @Test
    public void testAuthenticateNotValidUser() throws Exception {
	User user = createJohnDoe();
	user.save();

	Auth auth = new Auth();
	auth.email = "notJhon@doe.com";
	auth.password = user.password;

	assertThat(auth.validate()).isEqualTo("Invalid user or password");
    }
}
