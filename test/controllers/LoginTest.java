package controllers;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.GET;
import static play.test.Helpers.callAction;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.contentType;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.routeAndCall;
import static play.test.Helpers.status;
import helper.BaseModelTest;
import helper.TestSetup;
import models.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import play.api.mvc.Session;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Http.Context;
import utils.Helper;

@RunWith(MockitoJUnitRunner.class)
public class LoginTest extends BaseModelTest {

    @Mock
    Form<User> userForm;

    @Mock
    Form<Auth> authForm;

    Login controller = new Login() {
	Status handleBadRequest() {
	    return null;
	};

	Form<User> createUserForm() {
	    return userForm;
	};

	Form<Auth> createAuthForm() {
	    return authForm;
	};
    };

    @Test
    public void testActionShowRegistration() throws Exception {
	Result result = callAction(controllers.routes.ref.Login
		.showRegistration());
	assertThat(contentType(result)).as("text/html");
	assertThat(contentAsString(result)).contains("name");
	assertThat(contentAsString(result)).contains("vorname");
	assertThat(contentAsString(result)).contains("email");
	assertThat(contentAsString(result)).contains("password");
	assertThat(contentAsString(result)).contains("Registrieren");
    }

    @Test
    public void testActionShowLogin() throws Exception {
	Result result = callAction(controllers.routes.ref.Login.login());
	assertThat(contentType(result)).as("text/html");
	assertThat(contentAsString(result)).contains("email");
	assertThat(contentAsString(result)).contains("password");
	assertThat(contentAsString(result)).contains("Login");
	assertThat(contentAsString(result)).contains("Registrieren");

    }

    @Test
    public void testRouteShowRegistration() throws Exception {
	Result result = routeAndCall(fakeRequest(GET, "/showRegisration"));
	assertThat(status(result)).isEqualTo(OK);
    }

    @Test
    public void testRouteShowLogin() throws Exception {
	Result result = routeAndCall(fakeRequest(GET, "/login"));
	assertThat(status(result)).isEqualTo(OK);
    }

    @Test
    public void testRegistUser() throws Exception {
	User user = createJohnDoe();
	when(userForm.bindFromRequest()).thenReturn(userForm);
	when(userForm.get()).thenReturn(user);

	controller.executeRegistration();

	verify(userForm).get();
	User registredUser = User.find.byId(user.email);
	assertThat(registredUser).isNotNull();
	assertThat(registredUser.password).isEqualTo(user.password);
    }

    @Test
    public void testLoginUser() throws Exception {
	Context context = TestSetup.testHttpContext();
	Context.current.set(context);
	User user = createJohnDoe();
	user.save();
	when(authForm.bindFromRequest()).thenReturn(authForm);
	when(authForm.hasErrors()).thenReturn(Boolean.FALSE);
	Auth auth = new Auth();
	auth.email = user.email;
	auth.password = user.password;
	when(authForm.get()).thenReturn(auth);
	Result result = controller.authenticate();
	verify(context).session();
	verify(authForm).get();
    }

    @Test
    public void testLogoutUser() throws Exception {
	Context context = TestSetup.testHttpContext();
	Context.current.set(context);
	play.mvc.Http.Session session = context.session();
	controller.logout();
	verify(session).clear();
    }

    @Test
    public void testRegistInvalidUser() throws Exception {
	when(userForm.bindFromRequest()).thenReturn(userForm);
	User user = createJohnDoe();
	when(userForm.get()).thenReturn(user);
	when(userForm.hasErrors()).thenReturn(Boolean.TRUE);

	controller.executeRegistration();

	User registredUser = User.find.byId(user.email);
	assertThat(registredUser).isNull();
    }
    
    @Test
    public void testWhenLoggedInReturnLogout() {
	Context ctx = TestSetup.testHttpContext();
	Context.current.set(ctx);
	String logout = Helper.showLogText(ctx.session());
	assertThat(logout).isEqualTo("logout");
    }

}
