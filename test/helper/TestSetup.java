package helper;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import play.api.mvc.Request;
import play.api.mvc.RequestHeader;
import play.i18n.Lang;
import play.mvc.Http.Context;
import play.mvc.Http.Flash;
import play.mvc.Http.Session;

public class TestSetup {

    public static Context testHttpContext() {
	HashMap<String, String> emptyData = new HashMap<String, String>();

	Request request = mock(Request.class);
	when(request.host()).thenReturn("localhost");
	RequestHeader rh = mock(RequestHeader.class);
	when(rh.host()).thenReturn("localhost");

	Context ctx = mock(Context.class);
	when(ctx._requestHeader()).thenReturn(rh);
	when(ctx.flash()).thenReturn(new Flash(emptyData));
	when(ctx.lang()).thenReturn(Lang.forCode("en"));
	Session session = mock(Session.class);
	when(ctx.session()).thenReturn(session);
	when(session.get("email")).thenReturn("john.doe@doe.com");
	return ctx;

    }
}
