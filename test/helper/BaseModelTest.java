package helper;

import models.User;

import org.junit.After;
import org.junit.Before;

import play.test.FakeApplication;
import play.test.Helpers;

public class BaseModelTest {
    public static FakeApplication app;

    @Before
    public void startApp() {
	app = Helpers.fakeApplication(Helpers.inMemoryDatabase());
	Helpers.start(app);
    }

    @After
    public void stopApp() {
	Helpers.stop(app);
    }

    public static final User createJohnDoe() {
	User user = new User();
	String name = "Doe";
	user.name = name;
	String vorname = "John";
	user.vorname = vorname;
	String email = "john.doe@doe.com";
	user.email = email;
	String password = "secret";
	user.password = password;
	return user;
    }
}
