package views;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.contentType;
import static play.data.Form.*;
import models.User;

import org.junit.Test;

import play.api.templates.Html;
import play.data.Form;

public class RegistrationViewTest {

    @Test
    public void testShowRegistration() throws Exception {
	Form<User> userForm = form(User.class);
	Html content = views.html.registration.render(userForm);
	assertThat(contentType(content)).as("text/html");
	assertThat(contentAsString(content)).contains("name");
	assertThat(contentAsString(content)).contains("vorname");
	assertThat(contentAsString(content)).contains("email");
	assertThat(contentAsString(content)).contains("password");
	assertThat(contentAsString(content)).contains("Registrieren");
    }

}
