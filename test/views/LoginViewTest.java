package views;

import static org.fest.assertions.Assertions.assertThat;
import static play.data.Form.form;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.contentType;
import helper.TestSetup;

import org.junit.Test;

import play.api.templates.Html;
import play.data.Form;
import play.mvc.Http.Context;
import controllers.Auth;

public class LoginViewTest {
    @Test
    public void testShowLogin() throws Exception {
	Context.current.set(TestSetup.testHttpContext());
	Form<Auth> userForm = form(Auth.class);
	Html content = views.html.login.render(userForm);
	assertThat(contentType(content)).as("text/html");
	assertThat(contentAsString(content)).contains("email");
	assertThat(contentAsString(content)).contains("password");
	assertThat(contentAsString(content)).contains("Login");
	assertThat(contentAsString(content)).contains("Registrieren");
    }
}
